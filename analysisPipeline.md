1. Rerun the analyses with no infinities, and run likelihood ratio
   tests. Build tables of Likelihood ratio statistical tests.
   **AICcomparison.R**
2. Stats on overall effect sizes from hierarchical models.
   **EffectSizeSummaryStats.R**
3. Build LaTeX tables
   **MakeLikelihoodRatioLatex.R**

* skippedRuns.txt probably has multiple attempts appended onto
  it. Look into those results and get an accurate list. 
  *This is true! The code to get an accurate list is in
  code/fixskippedRuns.R, and the updated lists are in csv format in
  skippedRuns2.csv in each of the folders.*
**TODO: have PredictImportance remove file if exists before appending,
  to address this problem in the future?**
* Fix Violins.R too make sure that there is no hardcodng that messes
  up when there are missing runs
  *Done*
- Try removing data points (DONE) and also runs involving infinities, and run the analysis
- Get numbers and violins for Tim:
  - violins for empirical/simulated, removal/perturbation, immigration/closed
  - My interpretation of the results (what is important, etc.)
  - table of runs removed per web/model due to lack of mean-variance relationship
  - table of runs removed per web/model due to infinities
**TODO: if there is anything salvageable in this project, get the Rcpp
version of the code working**

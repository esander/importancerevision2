myArgs <- commandArgs(trailingOnly = TRUE)
web <- myArgs[1]
type <- myArgs[2]
imm <- as.numeric(myArgs[3])

library(predictimportance, lib.loc = '../../')
## library(predictimportance)
print(imm)
if(type == 'empirical'){
    runScriptsEmpirical(web = web, path = './', Immigration = imm)
} else {
    runScriptsModel(model = web, path = './', Immigration = imm)
}
